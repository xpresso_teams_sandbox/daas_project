from datetime import datetime

a = datetime.today().strftime('%Y-%m-%d')
b = datetime.today().strftime('%Y%m%d')

treasury_yield_link = 'https://www.federalreserve.gov/datadownload/Output.aspx?rel=H15&series=bf17364827e38702b42a58cf8eaa3f78&lastobs=&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package'

#VIX_link_daily_datahub = 'https://datahub.io/core/finance-vix/r/vix-daily.csv'

VIX_link_before_2004_cboe = 'http://www.cboe.com/publish/scheduledtask/mktdata/datahouse/vixarchive.xls'
VIX_link_daily_cboe = 'http://www.cboe.com/publish/scheduledtask/mktdata/datahouse/vixcurrent.csv'

#HPI_link_monthly_fhfa = 'https://www.fhfa.gov/HPI_master.csv'
HPI_link_monthly_fred = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=CSUSHPISA&scale=left&cosd=1990-01-01&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Monthly&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1987-01-01'.format(a,a,a)

GDP_link_quarterly_fred = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=GDPC1&scale=left&cosd=1990-01-01&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Quarterly&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1947-01-01'.format(a,a,a)

TED_spread_link_daily = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=TEDRATE&scale=left&cosd=1989-12-31&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Daily&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1986-01-02'.format(a,a,a)

BAA_corporate_bond_yield_sentiment_monthly_fred = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=BAA&scale=left&cosd=1990-01-01&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Monthly&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1919-01-01'.format(a,a,a)

Household_debt_link_quarterly_federal_reserve = 'https://www.federalreserve.gov/datadownload/Output.aspx?rel=FOR&series=5c8df3fd05e5b5ad4297328218040855&lastobs=20&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package'
Household_debt_link_quarterly_fred = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=TDSP&scale=left&cosd=1990-01-01&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Quarterly&fam=avg&fgst=lin&fgsnd=2020-01-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1980-01-01'.format(a,a,a)
FODSP_link = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=FODSP&scale=left&cosd=1990-01-01&coed={}&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Quarterly&fam=avg&fgst=lin&fgsnd=2020-01-01&line_index=1&transformation=lin&vintage_date={}&revision_date={}&nd=1980-01-01'.format(a,a,a)

oil_WTI_link_daily_datahub = 'https://datahub.io/core/oil-prices/r/wti-daily.csv'
#oil_WTI_link_daily_fred  = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgcolor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffffff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_tooltip=yes&id=DCOILWTICO&scale=left&cosd=1990-01-01&coed=2020-08-03&line_color=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&ost=-99999&oet=99999&mma=0&fml=a&fq=Daily&fam=avg&fgst=lin&fgsnd=2020-02-01&line_index=1&transformation=lin&vintage_date=2020-08-12&revision_date=2020-08-12&nd=1986-01-02'
#oil_WTI_link_weekly_datahub = 'https://datahub.io/core/oil-prices/r/wti-week.csv'
#oil_WTI_link_monthly_datahub = 'https://datahub.io/core/oil-prices/r/wti-month.csv'
#oil_WTI_link_yearly_datahub = 'https://datahub.io/core/oil-prices/r/wti-year.csv'

#gold_price_link_daily_stooq = 'https://stooq.com/q/d/l/?s=xauusd&d1=19900101&d2={}&i=d'.format(b)
gold_price_link_monthly_stooq = 'https://stooq.com/q/d/l/?s=xauusd&d1=19900101&d2={}&i=m'.format(b)
#gold_price_link_monthly_datahub = 'https://datahub.io/core/gold-prices/r/monthly.csv'


Consumer_Sentiment_Index_link_monthly_sca = 'http://www.sca.isr.umich.edu/files/tbmics.csv'
#Consumer_Sentiment_Index_link_yearly_sca = 'http://www.sca.isr.umich.edu/files/tbyics.csv'


